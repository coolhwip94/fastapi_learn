# Fast API Learn
> This repo holds notes from a FastAPI tutorial

## Setup
---
- Create virtual env and install requirements
  ```
  python -m venv venv
  source venv/bin/activate
  pip install -r requirements.txt
  ```

## Running
---

- Run application with `uvicorn`
    ```
    uvicorn main:app --reload
    ```

## Documentation
---
- FastAPI provides multiple forms of documentation.
  - `Swagger` : http://`<localhost/ip_address/hostname>`/docs
  - `Redoc` : http://`<localhost/ip_address/hostname>`/redoc
  - `Raw OpenAPI Json` : http://`<localhost/ip_address/hostname>`/openapi.json

## References
---
- `FastAPI` : https://fastapi.tiangolo.com/tutorial/
<br>

- `Redoc` : https://github.com/Redocly/redoc
> Redoc is an open-source tool for generating documentation from OpenAPI (fka Swagger) definitions.

- `uvicorn` : https://www.uvicorn.org
> Uvicorn is a lightning-fast ASGI server implementation, using uvloop and httptools.

- `pydantic` : https://pydantic-docs.helpmanual.io
> Data validation and settings management using python type annotations.

## Extras
---
- `pydantic_reference/pydantic_example.py`
  - simple example of pydantic and the data validation it provides




