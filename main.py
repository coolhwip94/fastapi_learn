from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional
from enum import Enum
# create FastAPI instance
# this is referenced command used to run app
# uvicorn main:app --reload
app = FastAPI()

class ModelName(str, Enum):
    alexnet = "alexnet"
    resnet = "resnet"
    lenet = "lenet"


@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/greeting") # example of defining another path
async def greeting():
    return {"message" : "What a wonderful day it is!"}


@app.get("/items")
async def items():
    return [{"item_id": 1},{"item_id": 2}]

@app.get("/items/{item_id}")
async def item_id(item_id: int):
    return {"item_id": item_id}


### Order Matters ####
# Because path operations are evaluated in order, 
# you need to make sure that the path for /users/me 
# is declared before the one for /users/{user_id}

@app.get("/users/me")
async def read_user_me():
    return {"user_id": "the current user"}

@app.get("/users/{user_id}")
async def read_user(user_id: str):
    return {"user_id": user_id}


# Predefined values
# If you have a path operation that receives a path parameter, 
# but you want the possible valid path parameter values to be 
# predefined, you can use a standard Python Enum


class ModelName(str, Enum):
    alexnet = "alexnet"
    resnet = "resnet"
    lenet = "lenet"


@app.get("/models/{model_name}")
async def get_model(model_name: ModelName):
    if model_name == ModelName.alexnet:
        return {"model_name": model_name, "message": "Deep Learning FTW!"}

    if model_name.value == "lenet":
        return {"model_name": model_name, "message": "LeCNN all the images"}

    return {"model_name": model_name, "message": "Have some residuals"}



## Post request with model
# Schema definition
class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None


@app.post("/items")
async def create_item(item: Item):
    return item

