'''
This serves as an example for the pydantic package
'''
from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, ValidationError
import logging

logger = logging.getLogger('pydantic_example_logger')

class User(BaseModel):
    id: int  # annotated tells python this is required
    name = 'John Doe'  # inferred as string, since it has a default it is nto required
    signup_ts: Optional[datetime] = None  # datetime field, but not rquired
    friends: List[int] = []  # requires list of integers
    

if __name__ == "__main__":

    # Example Data
    external_data = {
        'id': '123',
        'signup_ts': '2019-06-01 12:22',
        'friends': [1, 2, '3'],
    }
    
    # create instance with example data
    user = User(**external_data)

    print(user.id)  # print attributes


    # Data validation

    try:
        user_data = {
            'id': '10',
            'friends': 'test',
            'signup_ts':'ads'
        }
        user = User(**user_data)
    except ValidationError:
        logger.exception('Validation Error occured')

        '''

        Validation Error
        Traceback (most recent call last):
        File "<ipython-input-43-6585ecc49db2>", line 7, in <module>
            user = User(**user_data)
        File "/Users/leetruong/Desktop/projects/fast_api_learn/venv/lib/python3.8/site-packages/pydantic/main.py", line 406, in __init__
            raise validation_error
        pydantic.error_wrappers.ValidationError: 2 validation errors for User
        signup_ts
        invalid datetime format (type=value_error.datetime)
        friends
        value is not a valid list (type=type_error.list)

        '''

        
